
import { segment } from 'oicq';
import fs from 'fs';

/**
 * 云崽插件库：https://gitee.com/yhArcadia/Yunzai-Bot-plugins-index （gitee）   https://github.com/yhArcadia/Yunzai-Bot-plugins-index  （github）
 * 插件目前仅在小范围点对点共享，请勿传播
 * 冰祈工作的杂谈铺:878268637
 */

const Botname = '冰祈';
const mag = '咏唱中...'
const timeout = 15000;

export const rule = {
  heis: {
    reg: "^(#|来点)+(黑丝)$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  },
   bais: {
    reg: "^(#|来点)+(白丝)$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  },
   jur: {
    reg: "^(#|来点)+(巨乳)$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  },
   mcn: {
    reg: "^(#|来点)+(网红|网红图集)$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  },
    jk: {
    reg: "^(#|来点)+(jk)$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  },
    zuk: {
    reg: "^(#|来点)+(足控|脚控)$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  },
    onoff: {
    reg: "^#*设置撤回(开启|关闭)$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  },
     Help: {
    reg: "^#*涩涩帮助$",  //匹配消息正则，命令正则
    priority: 50, //优先级，越小优先度越高
    describe: "", //【命令】功能说明
  }
}
  export async function Chehui(msgRes,e){
    if (timeout!=0 && msgRes && msgRes.message_id){
      let target = null;
      if (e.isGroup) {
        target = e.group;
      }else{
        target = e.friend;
      }	
    if (target != null){
        setTimeout(() => {
          target.recallMsg(msgRes.message_id);
        },timeout);
       }
    }
    }
  export async function heis(e){
    await e.reply(`${Botname}${mag}`)
    let heis = fs.readFileSync('./resources/result/heis.txt','UTF-8')
    heis = heis.split('\n')
    let num= Math.round(Math.random() * (heis.length -1))
    let msg = [
      segment.image(heis[num])
    ]
        let msgRes =await e.reply(msg);
        this.Chehui(msgRes,e)
        return true
      }
  export async function bais(e){
    await e.reply(`${Botname}${mag}`)
        let bais = fs.readFileSync('./resources/result/bais.txt','UTF-8')
        bais = bais.split('\n')
        let num= Math.round(Math.random() * (bais.length -1))
        let msg = [
          segment.image(bais[num])
        ]
        let msgRes =await e.reply(msg);
        this.Chehui(msgRes,e)
        return true
      }
  export async function jur(e){
    await e.reply(`${Botname}${mag}`)
    let jur = fs.readFileSync('./resources/result/jur.txt','UTF-8')
    jur = jur.split('\n')
    let num= Math.round(Math.random() * (jur.length -1))
    let msg = [
      segment.image(jur[num])
    ]
    let msgRes =await e.reply(msg);
    this.Chehui(msgRes,e)
    return true
  }
  export async function mcn(e){
    await e.reply(`${Botname}${mag}`)
    let mcn = fs.readFileSync('./resources/result/mcn.txt','UTF-8')
    mcn = mcn.split('\n')
    let num= Math.round(Math.random() * (mcn.length -1))
    let msg = [
      segment.image(mcn[num])
    ]
    let msgRes =await e.reply(msg);
    this.Chehui(msgRes,e)
    return true
  }
  export async function jk(e){
    await e.reply(`${Botname}${mag}`)
    let jk = fs.readFileSync('./resources/result/jk.txt','UTF-8')
    jk = jk.split('\n')
    let num= Math.round(Math.random() * (jk.length -1))
    let msg = [
      segment.image(jk[num])
    ]
    let msgRes =await e.reply(msg);
    this.Chehui(msgRes,e)
    return true
  }
  export async function zuk(e){
    await e.reply(`${Botname}${mag}`)
    let zuk = fs.readFileSync('./resources/result/zuk.txt','UTF-8')
    zuk = zuk.split('\n')
    let num= Math.round(Math.random() * (zuk.length -1))
    let msg = [
      segment.image(zuk[num])
    ]
    let msgRes =await e.reply(msg);
    this.Chehui(msgRes,e)
    return true
  }
  export async function onoff(e) {
    let onoff;
    if (e.msg.indexOf("设定") > -1) {
      onoff = e.msg.replace("设定撤回", "");
    } else if (e.msg.indexOf("设置") > -1) {
      onoff = e.msg.replace("设置撤回", "");
    }  if (onoff == '关闭' && e.isMaster) {
      e.reply(`自动撤回已关闭`);
      timeout = 0;
    }
  }
  export async function Help(e){
    const msg = [
      '已收藏以下好看的',
      '\n',
      '黑丝,',
      '白丝,',
      'jk,',
      '足控,',
      '巨乳,',
      '网红图集',
    ]
    await e.reply(msg,true)
  }

