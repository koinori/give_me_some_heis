import plugin from '../../lib/plugins/plugin.js'
import { segment } from 'oicq'
import fs from 'fs'

/**
 * 云崽插件库：https://gitee.com/yhArcadia/Yunzai-Bot-plugins-index （gitee）   https://github.com/yhArcadia/Yunzai-Bot-plugins-index  （github）
 * 插件目前仅在小范围点对点共享，请勿传播
 * 冰祈工作的杂谈铺:878268637
 */

let Botname = '冰祈';
let mag = '咏唱中...'
let timeout = 15000;

export class example extends plugin {
  constructor () {
    super({
      /** 功能名称 */
      name: '给我黑丝',
      /** 功能描述 */
      dsc: '麻溜滴，给我黑丝',
      event: 'message',
      /** 优先级，数字越小等级越高 */
      priority: 50,
      rule: [
        {
          /** 命令正则匹配 */
          reg: '^(#|来点)+(黑丝)$',
          /** 执行方法 */
          fnc: 'heis'
        },
        {
          reg:'^(#|来点)+(白丝)$',
          fnc:'bais'
        },
        {
          reg:'^(#|来点)+(巨乳)$',
          fnc:'jur'
        },
        {
          reg:'^(#|来点)+(网红|网红图集)$',
          fnc:'mcn'
        },
        {
          reg:'^(#|来点)+(jk)$',
          fnc:'jk'
        },
        {
          reg:'^(#|来点)+(足控|脚控)$',
          fnc:'zuk'
        },
        {
          reg:'^#*设定撤回(开启|关闭)',
          fnc:'onoff'
        },
        {
          reg:'^涩涩帮助',
          fnc:'Help'
        }
      ]
    })
  }

  async Chehui(msgRes,e){
    if (timeout!=0 && msgRes && msgRes.message_id){
      let target = null;
      if (e.isGroup) {
        target = e.group;
      }else{
        target = e.friend;
      }	
    if (target != null){
        setTimeout(() => {
          target.recallMsg(msgRes.message_id);
        },timeout);
       }
    }
  }


  async heis(e){
    await e.reply(`${Botname}${mag}`)
    let heis = fs.readFileSync('./resources/result/heis.txt','UTF-8')
    heis = heis.split('\n')
    let num= Math.round(Math.random() * (heis.length -1))
    let msg = [
      segment.image(heis[num])
    ]
    let msgRes =await e.reply(msg);
    if (!msgRes){return e.reply('咏唱失败了呢QwQ')}
    this.Chehui(msgRes,e)
    return true
  }


  async bais(e){
    await e.reply(`${Botname}${mag}`)
    let bais = fs.readFileSync('./resources/result/bais.txt','UTF-8')
    bais = bais.split('\n')
    let num= Math.round(Math.random() * (bais.length -1))
    let msg = [
      segment.image(bais[num])
    ]
    let msgRes =await e.reply(msg);
    if (!msgRes){return e.reply('咏唱失败了呢QwQ')}
    this.Chehui(msgRes,e)
    return true
  }


  async jur(e){
    await e.reply(`${Botname}${mag}`)
    let jur = fs.readFileSync('./resources/result/jur.txt','UTF-8')
    jur = jur.split('\n')
    let num= Math.round(Math.random() * (jur.length -1))
    let msg = [
      segment.image(jur[num])
    ]
    let msgRes =await e.reply(msg);
    if (!msgRes){return e.reply('咏唱失败了呢QwQ')}
    this.Chehui(msgRes,e)
    return true
  }


  async mcn(e){
    await e.reply(`${Botname}${mag}`)
    let mcn = fs.readFileSync('./resources/result/mcn.txt','UTF-8')
    mcn = mcn.split('\n')
    let num= Math.round(Math.random() * (mcn.length -1))
    let msg = [
      segment.image(mcn[num])
    ]
    let msgRes =await e.reply(msg);
    if (!msgRes){return e.reply('咏唱失败了呢QwQ')}
    this.Chehui(msgRes,e)
    return true
  }


  async jk(e){
    await e.reply(`${Botname}${mag}`)
    let jk = fs.readFileSync('./resources/result/jk.txt','UTF-8')
    jk = jk.split('\n')
    let num= Math.round(Math.random() * (jk.length -1))
    let msg = [
      segment.image(jk[num])
    ]
    let msgRes =await e.reply(msg);
    if (!msgRes){return e.reply('咏唱失败了呢QwQ')}
    this.Chehui(msgRes,e)
    return true
  }


  async zuk(e){
    await e.reply(`${Botname}${mag}`)
    let zuk = fs.readFileSync('./resources/result/zuk.txt','UTF-8')
    zuk = zuk.split('\n')
    let num= Math.round(Math.random() * (zuk.length -1))
    let msg = [
      segment.image(zuk[num])
    ]
    let msgRes =await e.reply(msg);
    if (!msgRes){return e.reply('咏唱失败了呢QwQ')}
    this.Chehui(msgRes,e)
    return true
  }


  async onoff(e) {
    let onoff;
    if (e.msg.indexOf("设定") > -1) {
      onoff = e.msg.replace("设定撤回", "");
    } else if (e.msg.indexOf("设置") > -1) {
      onoff = e.msg.replace("设置撤回", "");
    }  if (onoff == '关闭' && e.isMaster) {
      e.reply(`自动撤回已关闭`);
      timeout = 0;
    }
  }

  
  async Help(e){
    let msg = [
      '已收藏以下好看的',
      '\n',
      '黑丝,',
      '白丝,',
      'jk,',
      '足控,',
      '巨乳,',
      '网红图集',
    ]
    await e.reply(msg,true)
  }
}

